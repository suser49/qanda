<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::middleware('auth')->group(function () {
    Route::post('/answer-store', [App\Http\Controllers\AnsweresController::class, 'store'])->name('answer.add');
    Route::post('/reply-store', [App\Http\Controllers\RepliesController::class, 'store'])->name('reply.add');
    Route::get('/', [App\Http\Controllers\QuestionsController::class, 'index'])->name('home');
    Route::get('/ask-question', [App\Http\Controllers\QuestionsController::class, 'create'])->name('ask-question');
    Route::get('/{question}/{slug}', [App\Http\Controllers\QuestionsController::class, 'show'])->name('show-question');
    Route::post('/ask-question', [App\Http\Controllers\QuestionsController::class, 'store'])->name('store-question');
    Route::post('like', [App\Http\Controllers\LikeController::class, 'like'])->name('like');
    Route::delete('like', [App\Http\Controllers\LikeController::class, 'unlike'])->name('unlike');
});