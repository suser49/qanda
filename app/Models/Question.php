<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $guarded = [];

    use HasFactory;

    public function user() {
        return $this->belongsTo(User::class);
    }

    /* public function likes() {
        return $this->belongsToMany(User::class);
    }*/

    public function answeres() {
        return $this->hasMany(Answer::class);
    } 
}
