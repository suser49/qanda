<?php

namespace App\Models;
use App\Contracts\Likeable;
use App\Models\Concerns\Likes;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Answer extends Model implements Likeable
{
    use HasFactory;
    use Likes;
    protected $fillable = [
        'answer',
    ];
    public function user() {
        return $this->belongsTo(User::class);
    }
    public function replies() {
        return $this->hasMany(Reply::class);
    } 
}