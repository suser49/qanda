<?php

namespace App\Http\Controllers;
use App\Models\Question;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

class QuestionsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = auth()->user()->pluck('id');
       // dd($users);
        $questions = Question::whereIn('user_id', $users)->with('user')->latest()->Paginate(5);
        //dd($questions);
        return view('questions.index', compact('questions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $loggedInUser = auth()->user();
        $topics = ["Physics", "Chemestry", "Maths", "Other"];
        return view('questions.create', compact('loggedInUser', 'topics'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd(request('image'));
        //DB::table('users')->latest()->simplePaginate(10);
        
        $data = $request->validate([
            'another' => '',
            'question' => 'required',
            'topic' => 'required',
        ]);
        
        
        if ($request['image']) {
            $imagePath = request('image')->store('uploads', 'public');

            $image = Image::make(public_path("storage/{$imagePath}"));
            $image->save();
        }

        auth()->user()->questions()->create([
            'question' => $data['question'],
            'topic' => $data['topic'],
            'image' => $imagePath ?? "null",
            'slug' => \Str::slug($data['question'])
        ]);
        
        return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $question = Question::where('id', '=', $id)->FirstOrFail();
        $answeres = $question->answeres;
        return view('questions.show', compact('question', 'answeres'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
