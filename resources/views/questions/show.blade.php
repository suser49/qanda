@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-header">
            Detailed Question
        </div>
        <div class="card-body">
            <div class="h3">
                {{ $question->question }}
            </div>
            {{-- {{ dd($question->image )}} --}}
            @if($question->image != "null")
                <div class="row p-3">
                    <img src="/storage/{{ $question->image }}" class="w-100">
                </div>
            @endif
            By -{{ $question->user->name }}
            {{ $question->created_at->diffForHumans()}}
        </div>
        @if(count($answeres) > 0)
        <hr>
        <div class="card-body">
            @foreach($answeres as $answer)
            <li>{{ $answer->user->name}} ~ {{ $answer->answer }} 
                @ {{ $answer->created_at->diffForHumans() }}
                @include('like', ['model' => $answer])
                @if($answer->image != "null")
                    <div class="row p-5">
                        <img src="/storage/{{ $answer->image }}" class="w-100">
                    </div>
                @endif
                @if(count($answer->replies) > 0)
                    <div class="p-5">
                        @foreach($answer->replies as $reply)
                            {{ $reply->reply }} by {{ $reply->user->name }}<br>
                        @endforeach
                    </div>
                @endif
                <form action="{{ route('reply.add') }}"
                method="post">
                @csrf      
                <div class="form-group row m-2">
                    <input id="reply"
                            name="reply"
                            type="text" 
                            class="form-control @error('reply') is-invalid @enderror" 
                                name="reply" value="{{ old('reply') }}"
                                placeholder="Reply"
                                required autocomplete="reply" autofocus>

                        @error('reply')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    <input name="answer_id" type="hidden" value="{{ $answer->id }}"/>
                </div>
                <div class="form-group row m-2">
                    <button class="btn btn-primary btn-sm rounded-pill btn-block" type="submit">
                        <span class="font-weight-bold">Reply</span>
                    </button>
                </div>
                   
            </form><hr>
            </li>
            @endforeach
        </div>
        @endif
        <hr>
        <div class="card-body">
            <form action="{{ route('answer.add') }}"
                enctype="multipart/form-data"
                method="post">
                @csrf      
                <div class="form-group row m-2">
                    <textarea id="answer"
                            name="answer"
                            type="text" 
                            class="form-control @error('answer') is-invalid @enderror" 
                                name="name" value="{{ old('answer') }}"
                                placeholder="Answer"
                                required autocomplete="answer" autofocus>
                        </textarea>

                        @error('answer')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    <input name="question_id" type="hidden" value="{{ $question->id }}"/>
                </div>
                <div class="form-group row m-2">
                    <label for="image">Attach Image, In support of your answer!</label>
                        <input type="file"
                            name="image"
                            class="form-control-file" 
                            id="image">
                    @error('image')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <hr>
                <div class="form-group row m-2">
                    <button class="btn btn-primary btn-sm rounded-pill btn-block" type="submit">
                        <span class="font-weight-bold">Answer</span>
                    </button>
                </div>
                   
            </form>
        </div>
    </div>
   
</div>
@endsection
