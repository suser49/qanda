@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-header">
            <strong>Write Question</strong>
        </div>

        <div class="card-body">
            <form method="POST" action="{{ route('store-question') }}" enctype="multipart/form-data">
                @csrf
                <div class="form-group row">
                    <div class="col-md-12">
                        <textarea id="question"
                            name="question"
                            type="text" 
                            class="form-control @error('question') is-invalid 
                            @enderror" name="name" value="{{ old('question') }}"
                                placeholder="Type Question"
                                required autocomplete="question" autofocus>
                        </textarea>

                        @error('question')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-12">
                        <select
                            id="topic"
                            name="topic"
                            class="form-select form-select-sm form-control"
                            aria-label=".form-select-sm example">
                            <option selected>Choose Topic</option>
                            @foreach($topics as $topic)
                                <option value="{{ $topic }}">{{ $topic }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-12">
                        <label for="image">Attach Image, In support of your question!</label>
                        <input type="file"
                            name="image"
                            class="form-control-file" 
                            id="image">
                    </div>
                    @error('image')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                
                <div class="form-group row">
                    <div class="col-md-12">
                        <input type="submit" class="btn btn-primary btn-lg btn-block">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
