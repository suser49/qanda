@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <strong>Do you have any questions ?</strong>
                    <a href="{{ route('ask-question') }}">
                        <span class="btn btn-primary btn-sm">Ask</span>
                    </a>
                </div>

                <div class="card-body">
                    <strong>List of Questions</strong>
                    <hr>
                    @if(count($questions) > 0)
                        <div>
                            <ul>
                                @foreach($questions as $question)
                                    <li>
                                        <a href="/{{ $question->id . '/' . $question->slug }}" 
                                            class="text-decoration-none text-capitalize text-bold text-dark">
                                            Question - {{ $question->question }}<br>
                                            Topic - {{ $question->topic }}<br>
                                            Ask by - {{ $question->user->name }}<br>
                                            Time - {{ $question->created_at->diffForHumans() }}
                                        </a>
                                        <hr>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="d-flex justify-content-center">
                            {{ $questions->links() }}
                        </div>
                    @else
                        <div class="alert alert-info">
                            There is no Question available! 
                            <a href="{{ route('ask-question') }}">Ask Now</a>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
