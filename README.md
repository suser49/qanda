Now,just clone your project in your preferred location.

Open your project in PhpStorm (or any other IDE you prefer).

copy .evn.example file in a new .evn file in that folder. Edit DB_DATABASE, DB_USERNAME, DB_PASSWORD

open terminal.

1. Type composer update

2. Type php artisan key:generate

3. Type php artisan config:clear

4. Type php artisan route:clear

5. Type php artisan view:clear

6. Type php artisan migrate

7. Type npm install && npm run dev

8. Type php artisan storage:link

9. Type php artisan serve

Open browser and type localhost:8000